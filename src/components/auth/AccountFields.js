import React from 'react'
import {reduxForm} from 'redux-form'
import {CheckboxField, PhoneFieldWithoutCountryCode, TextField, SelectField, CountryCode, Country} from './../forms'
var _ = require('lodash/core');

const validate = values => {
  const required = value => value ? null : 'Required'
  //const length = password => values.length <= 6 ? 'Good strength' : 'Password should have atleast 6 letters'
  const emailregex = email => (/^[-a-z0-9~!$%^&*_=+}{'?]+(\.[-a-z0-9~!$%^&*_=+}{'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i.test(values.email || '')) ? null : 'Invalid email'
  const errors = {}
  
  errors.password = required(values.password)
  //errors.password = length(values.password)

  if (!values.confirm_password) {
    errors.confirm_password = 'Required'
  } else if (values.confirm_password !== values.password) {
    errors.confirm_password = 'Password dont match'
  }
  errors.email = emailregex(values.email || '')
  if (!values.email) {
    errors.email = 'Required'
  } else if (emailregex(values.email)) {
    errors.email = 'Invalid Email'
  }
  
  if (values.password) {
    errors.password = (values.password).length > 5 ? null : 'Password should be atleast 6 letters'
  }
  return errors
}


const AccountFields = reduxForm({
  form: 'account fields',
  fields: ['email', 'password', 'confirm_password'],
  validate
})(React.createClass({
  render() {
      const {
      fields: {email, password, confirm_password},
      handleSubmit,
      submitting,
      onClosed,
      error
    } = this.props
     var data = {
      email     : this.props.values.email,
      password : this.props.values.password,
      password_confirmation    : this.props.values.password_confirmation,
    }
    return (
      <form className={`ui form ${submitting ? 'loading' : ''}`} onSubmit={(data)=>handleSubmit(data)}>
          <div className="field">
                <label>Email</label>
                <input type="text" placeholder="Email" {...email}/>
                {email.touched && email.error && <div className="ui pointing red basic label">{email.error}</div>}
          </div>
          <div className="field">
                <label>Pasword</label>
                <input type="password" placeholder="Pasword" {...password}/>
                {password.touched && password.error && <div className="ui pointing red basic label">{password.error}</div>}
          </div>
          <div className="field">
                <label>Confirm Password</label>
                <input type="password" placeholder="Confirm Password" {...confirm_password}/>
                {confirm_password.touched && confirm_password.error && <div className="ui pointing red basic label">{confirm_password.error}</div>}
          </div>  
          <div className="form-footer">
            <button className="btn -primary pull-right">Save &amp; Continue</button>
          </div>
      </form>
    )
  }
}))

module.exports = AccountFields