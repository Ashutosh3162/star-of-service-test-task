import React from 'react'
import {reduxForm} from 'redux-form'
import {RadioField, SelectField} from './../forms'

const validate = values => {
  console.log(values.date);
  const required = value => value ? null : 'Required';



  //const date_val = date => value => value > 31  ? 'Please enter a valid date' : null;

  const errors = {}
  errors.gender = required(values.gender)

  //errors.date=date_val(values.date)


  if (!values.date) {
    errors.date = 'Required'
  } 
  if ( values.date > 31) {
    errors.date = 'Please enter a valid date'
  }

  if (!values.month) {
    errors.month = 'Required'
  } 
  if (values.month > 12) {
    errors.month = 'Please enter a valid month'
  }

  if (!values.year) {
    errors.year = 'Required'
  } else if ((values.year).length !== 4) {
    errors.year = 'Please enter a valid year'
  }

  
  return errors
}

const PersonalFields = reduxForm({
  form: 'personalFields',
  fields: ['date', 'month', 'year', 'date_of_birth', 'gender', 'how_hear_about_us'],
  validate
})(React.createClass({
  render() {
    const {
      fields: {date, month, year, date_of_birth, gender, how_hear_about_us},
      handleSubmit,
      submitting,
      onClosed,
      error
    } = this.props
      var full_date = this.props.values.year+"."+this.props.values.month+"."+this.props.values.date      
      var data = {
        date_of_birth : (new Date(full_date).getTime() / 1000) ,
        gender : this.props.values.gender,
        how_hear_about_us : this.props.values.how_hear_about_us,
      }
    return (
      <form className={`ui form ${submitting ? 'loading' : ''}`} onSubmit={(data)=>handleSubmit(data)}>
          <div className="three fields">
          <div className="field">
                <label>Date</label>
                <input type="text" placeholder="Date" {...date}/>
                {date.touched && date.error && <div className="ui pointing red basic label">{date.error}</div>}
          </div>
          <div className="field">
                <label>Month</label>
                <input type="text" placeholder="Month" {...month}/>
                {month.touched && month.error && <div className="ui pointing red basic label">{month.error}</div>}
          </div>
          <div className="field">
                <label>Year</label>
                <input type="text" placeholder="Year" {...year}/>
                {year.touched && year.error && <div className="ui pointing red basic label">{year.error}</div>}
          </div>
          </div>
          <div className="field">
            <RadioField {...gender} label="Gender"
             values={{'Female': 'Female', 'Male': 'Male', 'Unspecified': 'Unspecified'}}/>
          </div>

          <div className="field">
            <SelectField options={[['Facebook', 'Facebook'], ['News', 'News'],['Twitter', 'Twitter']]}
             placeholder="Where Did You Hear About Us ?" label="" {...how_hear_about_us}/>
          </div>

          <div className="form-footer">
            <button className="btn -primary pull-right" onClick={this.previousStep}>Back</button>
            <button className="btn -primary pull-right" onClick={this.nextStep}>Save &amp; Continue</button>
          </div>
      </form>
    )
  },
  previousStep: function(e) {
    e.preventDefault()
    this.props.previousStep()
  },
  onChange: function(){
    console.log("element")
  }
}))

module.exports = PersonalFields