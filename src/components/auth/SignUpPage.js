/* global $*/
import React from 'react'
import {connect} from 'react-redux'
import AccountFields from './AccountFields'
import PersonalFields from './PersonalFields'
import Welcome from './Welcome'
import {push} from 'react-router-redux'
import assign from 'object-assign'
import Progress from 'react-progressbar'
import {updatefieldvalue} from '../../models/auth';

var fieldValues = {  
  email    : null,
  password : null,
  password_confirmation: null,
  date_of_birth: null,
  gender: null,
  how_hear_about_us: null
}

const SignUpPage = React.createClass({
  getInitialState () {
    return ({
      showErrorPopup: false,
      error_message: null,
      step : 1
    })
  },


  saveValues(field_value) {
    console.log('2222222')
    this.props.updateField(fieldValues)
    return function() {
      fieldValues = assign({}, fieldValues, field_value)
    }.bind(this)()
  },

  nextStep() {
    this.setState({
      step : this.state.step + 1
    })
  },

  previousStep() {
    this.setState({
      step : this.state.step - 1
    })
  },
  submitAccounts(data) {
  
      console.log('111111111', this.props)
    this.saveValues(data)
    this.nextStep()
  },

  showStep() {
    switch (this.state.step) {
      case 1:
        return <AccountFields onSubmit={(values) => this.submitAccounts(values)} fieldValues={fieldValues}
                              nextStep={this.nextStep}
                              previousStep={this.previousStep}
                              saveValues={this.saveValues} />
      case 2:
        return <PersonalFields  onSubmit={(values) => this.submitAccounts(values)} fieldValues={fieldValues}
                              nextStep={this.nextStep}
                              previousStep={this.previousStep}
                              saveValues={this.saveValues} /> 
      case 3:
        return <Welcome />                                                
    }
  },

  // on url load, first we dispatch an action
  componentWillMount () {
    console.log('SignUpPage.componentWillMount')
  },
  componentDidMount () {
    console.log('SignUpPage.componentDidMount')
  },
  closeErrorPopup () {
    this.setState({showErrorPopup: false, error_message: null})
  },
  render () {
    var percent = (this.state.step / 3 * 100)
    console.log("percentage====>",percent);
    return (      
      <div>      
        { this.state.step == 3 ? "Thank you!" : "Sign up" }
        <Progress completed={percent} />
          {
            this.showStep()
           // {<SignUpUserForm onSubmit={(values) => this.props.signUpRequest(values)} onClosed={this.props.onClosed} />}
          // {this.state.showErrorPopup && <ErrorPopup message={this.state.error_message} onClosed={this.props.onClosed} Error={this.state.showErrorPopup} />}
           }
      </div>
    )
  }
})

const ErrorPopup = React.createClass({
  render () {
    return (
      <div className="ui modal tiny text-center ">
        <div className="ui top attached message">
          <h2>Dashboard</h2>
        </div>
        <div className="ui middle attached segment">
          <p>{this.props.message}</p>
        </div>
      <div className="ui bottom attached segment">
          <button className="ui primary button" onClick={this.props.onClosed}>Ok</button>
        </div>
      </div>
    )
  },
  componentDidMount () {
    $('.ui.modal')
      .modal({
        detachable: false,
        closable: true
      })
      .modal('show')
  },
  componentWillUnmount () {
    const isActive = $('.ui.modal').modal('is active')
    if (isActive) {
      $('.ui.modal').modal('hide')
    }
  }
})

export default connect(state => ({
}), dispatch => ({
  signUpRequest: (values) => dispatch(push('/loginSent')),
  onClosed: () => dispatch(push('/login')),
  updateField: (values) => dispatch(updatefieldvalue(values))
}))(SignUpPage)
