import React from 'react'
import thank_img from '../../images/thanks.png'  

var Welcome = React.createClass({
  render: function() {
    return (
      <div>       
       <img src={thank_img} height="220"/>

       <button> Go to Dashboard </button>
      </div>
    )
  }
})

module.exports = Welcome